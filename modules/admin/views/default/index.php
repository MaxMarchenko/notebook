<?php

use yii\helpers\Url;

?>

<h1>Админка</h1>

<a href="<?= Url::to(['default/create']) ?>" class="btn btn-primary">Create</a>

<table class="table table-stripped">
    <thead>
    <tr>
        <td>#</td>
        <td>Название</td>
        <td>Действие</td>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($one as $item): ?>
        <tr>
            <td><?= $item->id ?></td>
            <td><?= $item->title ?></td>
            <td>
                <a href="<?= Url::to(['default/edit', 'id' => $item->id]) ?>" class="btn btn-success">Редактировать</a>
                <a href="<?= Url::to(['default/delete', 'id' => $item->id]) ?>" class="btn btn-danger">Удалить</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>