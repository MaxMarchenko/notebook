<?php

namespace app\modules\admin\controllers;

use app\models\MyList;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    public function actionIndex() {
        $one = MyList::getAll();;

        return $this->render('index', [
            'one' => $one,
        ]);
    }

    public function actionCreate() {
        $model = new MyList();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect('index');
            }

            return $this->render('create', ['model' => $model]);
        }

    public function actionEdit($id) {
        $one = MyList::getOne($id);

        if ($one->load(Yii::$app->request->post()) && $one->save()) {
            return $this->redirect('index');
        }

        return $this->render('edit', [
            'one' => $one,
            'id' => $id
        ]);
    }

    public function actionDelete($id) {
        $model = MyList::getOne($id);
        $model->delete();
        return $this->redirect('index');
    }
}
