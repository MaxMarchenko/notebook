<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

?>

<h1><?= $title ?></h1>
<div class="col-md-6">
    <ol>
        <?php foreach ($data as $item): ?>
            <li><a href="<?= Url::to(['view', 'id' => $item->id]) ?>"><?= $item->title ?></a></li>
        <?php endforeach; ?>
    </ol>
</div>