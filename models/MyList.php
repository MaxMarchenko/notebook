<?php

namespace app\models;

use yii\db\ActiveRecord;

class MyList extends ActiveRecord {

        public function rules()
        {
            return [
                [['title', 'description'], 'required'],
            ];
        }

    public static function tableName()
        {
            return 'list';
        }

        public static function getAll() {
            $data = MyList::find()->all();
            return $data;
        }

        public function getArray() {
            $array = [
                1 => 'Добро пожаловать на мой сайт',
                2 => 'Welcome to my site',
                3 =>  'Ласкаво прошу на мiй сайт'
            ];

            return $array;
        }

        public static function getOne($id) {
            $data = MyList::find()->where(['id' => $id])->one();
            return $data;
        }
}